#!/bin/bash

verifier_lettre() {
    lettre=$1
    fichier="lettres.txt"

    if grep -q "$lettre" "$fichier"; then
    	echo "0"
    else
    	echo "1"
    fi
}

# Utilisation de la fonction
verifier_lettre "$1" "lettres.txt"

