#!/bin/bash
#verifier si la lettre appartient bien au mot
function verification_lettre {
    local mot="$1"
    local lettre="$2"

    if [[ $mot == *"$lettre"* ]]; then
        echo "TRUE"
	return 0
    else
        echo "FALSE"
	return 1
    fi
}

# Appel de la fonction avec un exemple de mot et de lettre
verification_lettre "$1" "$2"


