#!/bin/bash
function bonhomme()
{
#tentatives ou $1 c'est les tentatives restantes
    if [ "$1" -eq 0 ]
    then
        echo " ==========Y= "
        echo " ||/       |  "
        echo " ||        0  "
        echo " ||       /|\ "
        echo " ||       /|  "
        echo "/||           "
        echo "=============="

    fi
    if [ "$1" -eq 1 ]
    then
        echo " ==========Y= "
        echo " ||/       |  "
        echo " ||        0  "
        echo " ||       /|\ "
        echo " ||       /   "
        echo "/||           "
        echo "=============="

    fi
    if [ "$1" -eq 2 ]
    then
        echo " ==========Y= "
        echo " ||/       |  "
        echo " ||        0  "
        echo " ||       /|\ "
        echo " ||           "
        echo "/||           "
        echo "=============="
    fi
    if [ "$1" -eq 3 ]
    then
        echo " ==========Y= "
        echo " ||/       |  "
        echo " ||        0  "
        echo " ||       /|  "
        echo " ||           "
        echo "/||           "
        echo "=============="
    fi
    if [ "$1" -eq 4 ]
    then
        echo " ==========Y= "
        echo " ||/       |  "
        echo " ||        0  "
        echo " ||        |  "
        echo " ||           "
        echo "/||           "
        echo "=============="
    fi
    if [ "$1" -eq 5 ]
    then
        echo " ==========Y= "
        echo " ||/       |  "
        echo " ||        0  "
        echo " ||           "
        echo " ||           "
        echo "/||           "
        echo "=============="
    fi
    if [ "$1" -eq 6 ]
    then
        echo " ==========Y= "
        echo " ||/       |  "
        echo " ||           "
        echo " ||           "
        echo " ||           "
        echo "/||           "
        echo "=============="
    fi
    if [ "$1" -eq 7 ]
    then
        echo " ==========Y= "
        echo " ||/          "
        echo " ||           "
        echo " ||           "
        echo " ||           "
        echo "/||           "
        echo "=============="
    fi
    if [ "$1" -eq 8 ]
    then
        echo "              "
        echo " ||/          "
        echo " ||           "
        echo " ||           "
        echo " ||           "
        echo "/||           "
        echo "=============="
    fi
    if [ "$1" -eq 9 ]
    then
        echo "              "
        echo "              "
        echo "              "
        echo "              "
        echo "              "
        echo "              "
        echo "=============="
    fi
        if [ "$1" -eq 10 ]
    then
        echo "              "
        echo "              "
        echo "              "
        echo "              "
        echo "              "
        echo "              "
        echo "              "
    fi
}

tentatives="$1"
bonhomme "$tentatives"
