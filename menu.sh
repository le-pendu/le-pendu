#! /usr/bin/env bash


# Fonction pour afficher le menu
function afficher_menu {
  clear
  echo "==============================="
  echo "        JEU DU PENDU           "
  echo "==============================="
  echo "1. Jouer"
  echo "-------------------------------"
  echo "2. Quitter"
  echo "==============================="
}

# Fonction qui affiche le choix de la difficulté
function afficher_choix_difficulte {
  clear
  echo "==============================="
  echo "    CHOIX DE LA DIFFICULTE     "
  echo "==============================="
  echo "1. Facile"
  echo "2. Intermédiaire"
  echo "3. Difficile"
  echo "-------------------------------"
  echo "4. Retour"
  echo "==============================="

}

function choix_difficulte {
  exit=0
  while [ $exit = 0 ]
  do
    afficher_choix_difficulte

    # Lire le choix de l'utilisateur
    read -p "Entrez votre choix (1-4) : " choix

    # Traitement du choix
    if [ $choix = 1 ]
    then
      echo "-------------------------------"
      echo "Petit joueur..."
      echo "-------------------------------"
      ./jeu.sh $(./tirage_mot.sh 1)
      exit=1

    elif [ $choix = 2 ]
    then
      echo "-------------------------------"
      echo "Commençons..."
      echo "-------------------------------"
      ./jeu.sh $(./tirage_mot.sh 2)
      exit=1

    elif [ $choix = 3 ]
    then
      echo "-------------------------------"
      echo "Vous êtes ambitieux"
      echo "-------------------------------"
      ./jeu.sh $(./tirage_mot.sh 3)
      exit=1

    elif [ $choix = 4 ]
    then
      exit=1

    else
      echo "Choix invalide. Veuillez entrer un nombre entre 1 et 4."

    fi

  done

}

# Boucle principale du menu
function menu {
  exit=0
  while [ $exit = 0 ];
  do
    afficher_menu

    # Lire le choix de l'utilisateur
    read -p "Entrez votre choix (1-2) : " choix

    # Traitement du choix
    if [ $choix = 1 ]
    then
      echo "-------------------------------"
      echo "Lancement du jeu..."
      echo "-------------------------------"
      choix_difficulte
      exit=0

    elif [ $choix = 2 ]
    then
      echo "-------------------------------"
      echo "Au revoir !"
      echo "-------------------------------"
      exit=1

    else
      echo "Choix invalide. Veuillez entrer un nombre entre 1 et 2."
    fi


    # Lire le choix de l'utilisateur
    read -p "Appuyez sur Entrée pour continuer..."
  done
}


menu
