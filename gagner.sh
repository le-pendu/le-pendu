#!/bin/bash

function gagner() {
    tentatives="$1"
    lettres="$2"

    if [ "$tentatives" -eq 0 ]; then
        echo 2
    fi

    for ((i = 0; i < ${#lettres}; i++)); do
        if [ "${lettres:$i:1}" == "_" ]; then
            echo 0
            return
        fi
    done

    echo 1
}

tentatives="$1"
lettres_trouvees="$2"

gagner "$tentatives" "$lettres_trouvees"
