#! /usr/bin/env bash

function verif_tentative {
    if [ $1 -gt 0 ]
    then
        echo "nombre tentative restantes : $1"
    elif [ $1 -eq 0 ]
    then
        echo "plus de tentatives disponibles !!"
    fi
}
verif_tentative $1
