#!/usr/bin/env bash


function affichage_jeu {
  clear
  echo "==============================="
  echo "        JEU DU PENDU           "
  echo "==============================="

  ./bonhomme.sh $1

  ./affichagemot_adeviner.sh $2

  ./affichage_lettres_deja_utilise.sh
  echo "==============================="

}

affichage_jeu $1 $2 
