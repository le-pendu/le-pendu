#!/bin/bash

function affichage_mot {
    local mot="$1"

    echo "==============================="
    echo "Lettres du mot à deviner :"
    echo "                               "
    echo "-------------------------------"
    for ((i=0; i<${#mot}; i++)); do
        lettre="${mot:$i:1}"
        echo -n "| $lettre "
    done
    echo "|"
    echo "-------------------------------"
    echo "                               "
    echo "==============================="
}
affichage_mot "$1"
