#! /usr/bin/env bash


function nouveau_mot {

  mot_base=$1
  ancien_mot=$2
  lettre=$3

  for ((i=0; i<${#mot_base}; i++))
  do

    if [ ${mot_base:$i:1} = $lettre ]
    then
      for ((j=0; j<${#ancien_mot}; j++))
      do
        if [ $i -eq $j ]
        then
          l="${mot_base:$j:1}"
          echo -n $l
        fi
      done
    else
      l="${ancien_mot:$i:1}"
      echo -n $l
    fi
  done
  echo
}

nouveau_mot $1 $2 $3
