#! /usr/bin/env bash

function init_mot {
    longueur=$1
    while [ $longueur -gt 0 ]
    do
        echo -n "_"
        longueur=$((longueur - 1))
    done
    echo
}

init_mot $1

