#! /usr/bin/env bash 

function tirage_mot {

if [ "$1" -eq 1 ]
then
	# génerer un nb de ligne aléatoire
	LIGNES=$(echo $(($RANDOM% 1601)))

	#selectionné un mot aléatoire
	MOT=$(head -n $LIGNES ./dict_dif1.txt | tail -1)
	echo $MOT

elif [ "$1" -eq 2 ]
then

        LIGNES=$(echo $(($RANDOM% 10152)))

        MOT=$(head -n $LIGNES ./dict_dif2.txt | tail -1)
        echo $MOT


elif [ "$1" -eq 3 ]
then 

        # génerer un nb de ligne aléatoire
        LIGNES=$(echo $(($RANDOM% 21766 )))

        #selectionné un mot aléatoire
        MOT=$(head -n $LIGNES ./dict_dif3.txt | tail -1)
        echo $MOT
fi
}

tirage_mot $1
