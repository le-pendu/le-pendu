#!/bin/bash
function affichage_lettre_deja_utilise {
    echo "Vous avez déjà utilisé les lettres suivantes :"
    lettres= echo $(tr -cd '[:alpha:]' < "lettres.txt" | fold -w1 | sort -u | paste -sd "|")
}
# Utilisation de la fonction
affichage_lettre_deja_utilise

